import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),

      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  bool mapView = false;

  List<dynamic> _vehicles = [];

  Future<List<dynamic>> getVehicles() async {
    if (_vehicles.isNotEmpty) return _vehicles;
    const url = "http://10.246.28.14:3000/vehicles";
    Response response = await get(url);
    _vehicles = json.decode(response.body);
    return _vehicles;
  }

  List<Marker> _markers = [];

  Future<List<Marker>> getMarkers() async {
    if (_markers.isNotEmpty) return _markers;
    BitmapDescriptor vehicleIcon = await iconToBitmap(icon: Icons.directions_car_rounded);

    _markers = _vehicles.map((vehicle) {
      return Marker(
        markerId: MarkerId(vehicle["id"].toString()),
        icon: vehicleIcon,
        position: LatLng(vehicle["latitude"], vehicle["longitude"]),
      );
    }).toList();

    return _markers;
  }

  Future<BitmapDescriptor> iconToBitmap({IconData icon, Color color = Colors.black, int size = 100}) async {
    // We create a Canvas with a PictureRecorder in order to render the image
    final pictureRecorder = PictureRecorder();
    final canvas = Canvas(pictureRecorder);
    // We need to paint the icon on canvas. We’ll do that using a TextPainter:
    final textPainter = TextPainter(textDirection: TextDirection.ltr);
    // TextPainter needs text to paint. So we need express our IconData as a String
    final iconStr = String.fromCharCode(icon.codePoint);
    // We need to specify icon's font family and define a size & color
    textPainter.text = TextSpan(
        text: iconStr,
        style: TextStyle(
          letterSpacing: 0.0,
          fontSize: size.toDouble(),
          fontFamily: icon.fontFamily,
          color: color,
        )
    );
    textPainter.layout();
    textPainter.paint(canvas, Offset(0.0, 0.0));
    // We need to encode the canvas as PNG data, so that the Marker can read it
    final picture = pictureRecorder.endRecording();
    final image = await picture.toImage(size, size);
    final bytes = await image.toByteData(format: ImageByteFormat.png);
    // Finally, we return BitmapDescriptor from our PNG data
    return BitmapDescriptor.fromBytes(bytes.buffer.asUint8List());
  }


  @override
  Widget build(BuildContext context) {

    Widget map = FutureBuilder(
        future: getMarkers(),
        builder: (context, snapshot) {
          return GoogleMap(
            initialCameraPosition: CameraPosition(
              target: LatLng(44.0143192, 1.3392535),
            ),
            markers: _markers.toSet(),
          );
        });

    Widget list = Padding(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: FutureBuilder(
          future: getVehicles(),
          builder: (context, snapshot) {
            return ListView.builder(
              itemCount: _vehicles.length,
              itemBuilder: (context, i) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    child: ListTile(
                      leading: Icon(Icons.directions_car_rounded),
                      title: Text("${_vehicles[i]["brand"]} ${_vehicles[i]["model"]}"),
                    ),
                  ),
                );
              },
            );
          },
        )
    );


    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: mapView ? map : list
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            mapView = !mapView;
            print("Switch to map: $mapView");
          });
        },
        tooltip: 'Switch view',
        child: Icon(mapView ? Icons.list : Icons.map),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
