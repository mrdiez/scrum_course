const express = require("express");
const app = express();

const sqlite = require("sqlite");
const sqlite3 = require("sqlite3");

(async () => {

    const db = await sqlite.open({
        driver: sqlite3.Database,
        filename: '/tmp/database.db'
    });

    // await db.exec(`
    
    // DROP TABLE IF EXISTS vehicles;
    
    // CREATE TABLE IF NOT EXISTS vehicles (
    //     id INT PRIMARY KEY,
    //     brand VARCHAR(50),
    //     model VARCHAR(50),
    //     registration VARCHAR(50),
    //     latitude FLOAT,
    //     longitude FLOAT
    // );

    // INSERT OR REPLACE INTO vehicles (id, brand, model, registration, latitude, longitude)
    // VALUES
    //     (1, 'Peugeot', '208', '123-ABC-123', 43.9837759, 1.3261691),
    //     (2, 'Ferrari', 'F430', '123-DEF-123', 43.9990101, 1.3488968),
    //     (3, 'Austin Martin', 'DB9', '123-GHI-123', 44.0091841, 1.3544103),
    //     (4, 'Renault', 'Twingo', '123-ABC-JKL', 43.7586997, 1.3050491),
    //     (5, 'Citroën', 'DS7 Crossback', '123-MNO-123', 43.6034404, 1.4355658),
    //     (6, 'Chevrolet', 'Camaro', '123-PQR-123', 43.6035003, 1.4355359),
    //     (7, 'Ford', 'Mustang', '123-STU-123', 38.9137584, 1.4475938),
    //     (8, 'Fiat', 'Multipla', '123-VWX-123', 48.8589507, 2.2770202),
    //     (9, 'Citroën', '2CV', '123-YZ-123', 40.7536171, -73.9937916);

    // `).then((result) => console.log('\nDatabase reset\n'), (e) => console.error(e));

    const vehicles = await db.all('SELECT * FROM vehicles');
    console.log(vehicles);


    app.listen(3000, () => console.log("\n SERVER STARTED \n"));
    app.get('/', (request, response) => {
        response.send("Hello world");
    });    
    app.get('/vehicles', (request, response) => {
        response.send(vehicles);
    });  
})();
